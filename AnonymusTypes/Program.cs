﻿/*Программа 1.

Создать четыре объекта анонимного типа для описания планет Солнечной системы со свойствами "Название", 
"Порядковый номер от Солнца", "Длина экватора", "Предыдущая планета" (ссылка на объект - предыдущую планету):

    Венера
    Земля
    Марс
    Венера (снова)
Данные по планетам взять из открытых источников.
Вывести в консоль информацию обо всех созданных "планетах". 
Рядом с информацией по каждой планете вывести эквивалентна ли она Венере.*/

namespace AnonymusTypes;

// задание невнятно сформулировано. Не понятно что за предыдущая планета и что нужно сделать по сути.
internal class Program
{
    static void Main()
    {
        var venus = new
        {
            Name = "Venus",
            NumberFromSun = 2,
            EquatorialCircumferenceKm = 38024.6,
            PreviousPlanet = default(object) // ?
        };
        var earth = new
        {
            Name = "Earth",
            NumberFromSun = 3,
            EquatorialCircumferenceKm = 40030.2,
            PreviousPlanet = venus
        };
        var mars = new
        {
            Name = "Mars",
            NumberFromSun = 4,
            EquatorialCircumferenceKm = 21296.9,
            PreviousPlanet = earth
        };
        var venus2 = new
        {
            Name = "Venus",
            NumberFromSun = 2,
            EquatorialCircumferenceKm = 38024.6,
            PreviousPlanet = mars // ?
        };

        Console.WriteLine($"Планета: {venus}");
        Console.WriteLine($"\tэквивалентна Венере: {venus.Equals(venus)}\n");
        Console.WriteLine($"Планета: {earth}");
        Console.WriteLine($"\tэквивалентна Венере: {earth.Equals(venus)}\n");
        Console.WriteLine($"Планета: {mars}");
        Console.WriteLine($"\tэквивалентна Венере: {mars.Equals(venus)}\n");
        Console.WriteLine($"Планета: {venus2}");
        Console.WriteLine($"\tэквивалентна Венере: {venus2.Equals(venus)}\n");
    }
}