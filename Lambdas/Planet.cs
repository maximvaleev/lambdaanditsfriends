﻿namespace Lambdas;

internal class Planet
{
    public string Name { get; private set; }
    public int NumberFromSun { get; private set; }
    public double EquatorialCircumferenceKm { get; private set; }
    public Planet? PreviousPlanet { get; private set; }

    public Planet(string name, int numberFromSun, double equatorialCircumferenceKm, Planet? previousPlanet)
    {
        Name = name;
        NumberFromSun = numberFromSun;
        EquatorialCircumferenceKm = equatorialCircumferenceKm;
        PreviousPlanet = previousPlanet;
    }
}
