﻿namespace Lambdas;

internal class PlanetCatalog
{
    readonly List<Planet> _planets = new();

    public PlanetCatalog()
    {
        Planet venus = new("Venus", 2, 38024.6, null);
        Planet earth = new("Earth", 3, 40030.2, venus);
        Planet mars = new("Mars", 4, 21296.9, earth);
        _planets.Add(venus);
        _planets.Add(earth);
        _planets.Add(mars);
    }

    public (int number, double equator, string? error) GetPlanet(string name, Func<string, string?> PlanetValidator)
    {
        string? error = PlanetValidator(name);
        if (error is not null) 
        {
            return (0, 0d, error);
        }
        Planet? planetFound = _planets.Find(x => x.Name == name);
        if (planetFound is not null)
        {
            return (planetFound.NumberFromSun, planetFound.EquatorialCircumferenceKm, null);
        }
        return (0, 0d, "Не удалось найти планету");
    }
}
